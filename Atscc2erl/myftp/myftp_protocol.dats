(*
** protocal for myftp
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

abstype ssftp // HX: it acts like a placeholder

(* ****** ****** *)
//
datavtype
chanpos_ssftp(type) =
| chanpos_ssftp_ls(ssftp_ls) of ()
//
| chanpos_ssftp_cd(ssftp_cd) of (string)
//
| chanpos_ssftp_get(ssftp_get) of (string)
//
(*
| chanpos_ssftp_put(ssftp_put) of (string)
*)
//
| chanpos_ssftp_quit(chnil) of ()
//
where
ssftp_ls = chsnd(ERLval)::ssconj(ssftp)
and
ssftp_cd = chsnd(ERLval)::ssconj(ssftp)
and
ssftp_get = chsnd(ERLval)::ssconj(ssftp)
(*
and
ssftp_put = chsnd(ERLval)::ssconj(ssftp)
*)
and
ssftp_quit = chnil
//
(* ****** ****** *)
//
extern
fun
chanpos_ssftp
(
  !chanpos(ssconj(ssftp)) >> chanpos(ss)
) : #[ss:type] chanpos_ssftp(ss) = "mac#"
//
implement
chanpos_ssftp(chp) = let
//
extern
prfun
__assert
(
  chp: !chanpos(ssconj(ssftp)) >> chanpos(chrcv(chanpos_ssftp(ss))::ss)
) : #[ss:type] void
//
prval () = __assert(chp)
//
in
  channel_recv(chp)
end // end of [chanpos_ssftp]
//
(* ****** ****** *)
//
extern
fun
channeg_ssftp_ls
  (!channeg(ssconj(ssftp)) >> channeg(ssftp_ls)): void = "mac#"
//
extern
fun
channeg_ssftp_cd
  (!channeg(ssconj(ssftp)) >> channeg(ssftp_cd), Dir: string): void = "mac#"
//
extern
fun
channeg_ssftp_get
  (!channeg(ssconj(ssftp)) >> channeg(ssftp_cd), Filename: string): void = "mac#"
//
and
channeg_ssftp_quit
  (!channeg(ssconj(ssftp)) >> channeg(ssftp_quit)): void = "mac#"
//
(* ****** ****** *)

implement
channeg_ssftp_ls
  (chn) = let
//
extern
prfun
__assert
(
  chn: !channeg(ssconj(ssftp)) >> channeg(chrcv(chanpos_ssftp(ssftp_ls))::ssftp_ls)
) : void
//
prval () = __assert(chn)
//
in
  channel_send(chn, chanpos_ssftp_ls())
end // end of [channeg_ssftp_ls]

implement
channeg_ssftp_cd
  (chn, Dir) = let
//
extern
prfun
__assert
(
  chn: !channeg(ssconj(ssftp)) >> channeg(chrcv(chanpos_ssftp(ssftp_cd))::ssftp_cd)
) : void
//
prval () = __assert(chn)
//
in
  channel_send(chn, chanpos_ssftp_cd(Dir))
end // end of [channeg_ssftp_cd]

implement
channeg_ssftp_get
  (chn, Filename) = let
//
extern
prfun
__assert
(
  chn: !channeg(ssconj(ssftp)) >> channeg(chrcv(chanpos_ssftp(ssftp_get))::ssftp_get)
) : void
//
prval () = __assert(chn)
//
in
  channel_send(chn, chanpos_ssftp_get(Filename))
end // end of [channeg_ssftp_get]

implement
channeg_ssftp_quit
  (chn) = let
//
extern
prfun
__assert
(
  chn: !channeg(ssconj(ssftp)) >> channeg(chrcv(chanpos_ssftp(ssftp_quit))::ssftp_quit)
) : void
//
prval () = __assert(chn)
//
in
  channel_send(chn, chanpos_ssftp_quit())
end // end of [channeg_ssftp_quit]

(* ****** ****** *)
//
macdef
SERVICE_MYFTP = $extval(atom, "'SERVICE_MYFTP'")
//
(* ****** ****** *)

(* end of [myftp_protocol.dats] *)
