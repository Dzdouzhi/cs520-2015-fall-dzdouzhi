(*
** server for myftp
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)
//
#define
ATS_EXTERN_PREFIX "myftp_"
#define
ATS_STATIC_PREFIX "_myftp_"
//
(* ****** ****** *)

%{^
%%
-module(myftp_server_dats).
%%
-export([main0_erl/0]).
%%
-compile(nowarn_unused_vars).
-compile(nowarn_unused_function).
%%
-export([ats2erlpre_cloref1_app/2]).
-export([ats2erlpre_cloref2_app/3]).
-export([libats2erl_session_chque_server/0]).
-export([libats2erl_session_chanpos_server/2]).
-export([libats2erl_session_channeg_server/2]).
-export([libats2erl_session_chansrvc_create_server/1]).
-export([libats2erl_session_chansrvc2_create_server/1]).
%%
-include("libatscc2erl_all.hrl").
-include("Session/mylibats2erl_all.hrl").
%%
%} // end of [%{]

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2ERL}/staloadall.hats"
//
staload
"{$LIBATSCC2ERL}/SATS/Erlang/file.sats"
//
staload
"{$LIBATSCC2ERL}/Session/SATS/basis.sats"
//
(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)
//
#include "./myftp_pass.dats"
#include "./myftp_protocol.dats"
//
(* ****** ****** *)
//
extern
fun
myftp_server(chanpos(ssconj(ssftp))): void
//
(* ****** ****** *)

implement
myftp_server(chp) = let
//
val opt = chanpos_ssftp(chp)
//
val () = println! ("myftp_server: opt = ", $UN.castvwtp1{ERLval}(opt))
//
in
//
case+ opt of
//
| ~chanpos_ssftp_ls() => let
    val Dir = "."
    val res =
      $extfcall(ERLval, "file:list_dir", Dir)
    // end of [val]
  in
    channel_send(chp, res); myftp_server(chp)
  end // end of [chanpos_ssftp_ls]
//
| ~chanpos_ssftp_cd(Dir) => let
    val res =
      $extfcall(ERLval, "file:set_cwd", Dir)
    // end of [val]
  in
    channel_send(chp, res); myftp_server(chp)
  end // end of [chanpos_ssftp_cd]
//
| ~chanpos_ssftp_get(Filename) => let
    val res =
      $extfcall(ERLval, "file:read_file", Filename)
    // end of [val]
  in
    channel_send(chp, res); myftp_server(chp)
  end // end of [chanpos_ssftp_get]
//
| ~chanpos_ssftp_quit() => chanpos_nil_wait(chp)
//
end // end of [myftp_server]

(* ****** ****** *)
//
assume pass_succ = ssconj(ssftp)
//
implement
pass_server$cont<> () =
  channeg_create(llam(chp) => myftp_server(chp))
//
(* ****** ****** *)

extern
fun
main0_erl
(
// argumentless
) : void = "mac#"
//
implement
main0_erl () =
{
//
val srvc =
  chansrvc_create(lam(chp) => pass_server(chp))
//
val ((*void*)) = chansrvc_register(SERVICE_MYFTP, srvc)
//
} (* end of [main0_erl] *)

(* ****** ****** *)

(* end of [myftp_server.dats] *)
