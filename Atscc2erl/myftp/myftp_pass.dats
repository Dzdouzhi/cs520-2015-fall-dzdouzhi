(*
** Password service
*)

(* ****** ****** *)

#ifdef
PASS_SUCC
#then
#else
abstype pass_succ
#endif

(* ****** ****** *)
//
datavtype
pass_resp =
  | PASS_fail of ()
  | PASS_succ of (channeg(pass_succ))
  | PASS_retry of (channeg(pass_init))
//
where
pass_init =
  chrcv(string)::chsnd(pass_resp)::chnil
//
(* ****** ****** *)
//
extern
fun{}
pass_server(chanpos(pass_init)): void
//
extern
fun{}
pass_client(channeg(pass_init)): Option_vt(channeg(pass_succ))
//
(* ****** ****** *)
//
extern
fun{}
pass_server$pass(string): bool
//
(* ****** ****** *)

implement
{}(*tmp*)
pass_server$pass
  (pass) =
(
case+ pass of
//
| "BU-CAS-CS520" => true
//
| _(*rest-of-string*) => false
//
) (* end of [pass_server$password] *)

(* ****** ****** *)
//
extern
fun{}
pass_server$cont(): channeg(pass_succ)
//
(* ****** ****** *)
//
implement
{}(*tmp*)
pass_server
  (chp) = let
//
fun
pass_server_retry
(
  n: int, chp: chanpos(pass_init)
) : void = let
//
val pass = channel_recv(chp)
val test = pass_server$pass(pass)
//
in
//
if
test
then (
  channel_send_close(chp, PASS_succ(pass_server$cont()))
) (* end of [then] *)
else (
  if n >= 2
    then channel_send_close(chp, PASS_fail())
    else channel_send_close(chp, PASS_retry(channeg_create(llam(chp) => pass_server_retry(n+1, chp))))
) (* end of [else] *)
//
end (* end of [pass_server_retry] *)
//
in
  pass_server_retry(0, chp)
end // end of [pass_server]

(* ****** ****** *)
//
extern
fun{}
pass_client$password((*void*)): string
//
implement
{}(*tmp*)
pass_client(chn) = let
//
val () =
channel_send
  (chn, pass_client$password())
//
in
//
case+
channel_recv_close(chn)
of // case+
| ~PASS_fail() => None_vt()
| ~PASS_succ(chn2) => Some_vt(chn2)
| ~PASS_retry(chn2) => pass_client(chn2)
//
end // end of [pass_client]

(* ****** ****** *)

(* end of [myftp_pass.dats] *)
