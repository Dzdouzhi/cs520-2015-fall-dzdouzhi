%%%%%%

-module(myftpd_stop).
-export([main0_erl/0]).

%%%%%%

main0_erl() -> rpc:call('myftpd@localhost', init, stop, []).

%%%%%% end of [myftpd_stop] %%%%%%
