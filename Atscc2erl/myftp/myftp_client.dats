(*
** client for myftp
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)
//
#define
ATS_EXTERN_PREFIX "myftp_"
#define
ATS_STATIC_PREFIX "_myftp_"
//
(* ****** ****** *)

%{^
%%
-module(myftp_client_dats).
%%
-export([main0_erl/0]).
%%
-compile(nowarn_unused_vars).
-compile(nowarn_unused_function).
%%
-export([ats2erlpre_cloref1_app/2]).
-export([ats2erlpre_cloref2_app/3]).
-export([ats2erlpre_ref_server_proc/1]).
-export([libats2erl_session_chque_server/0]).
-export([libats2erl_session_chanpos_server/2]).
-export([libats2erl_session_channeg_server/2]).
-export([libats2erl_session_chansrvc_create_server/1]).
-export([libats2erl_session_chansrvc2_create_server/1]).
%%
-include("libatscc2erl_all.hrl").
-include("Session/mylibats2erl_all.hrl").
%%
%} // end of [%{]

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2ERL}/staloadall.hats"
//
staload
"{$LIBATSCC2ERL}/SATS/Erlang/file.sats"
//
staload
"{$LIBATSCC2ERL}/Session/SATS/basis.sats"
//
(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)
//
#include "./myftp_pass.dats"
#include "./myftp_protocol.dats"
//
(* ****** ****** *)
//
extern
fun
myftp_client_ls(!channeg(ssconj(ssftp))): ERLval
extern
fun
myftp_client_cd(!channeg(ssconj(ssftp)), Dir: string): ERLval
extern
fun
myftp_client_get(!channeg(ssconj(ssftp)), Filename: string): ERLval
extern
fun
myftp_client_quit(channeg(ssconj(ssftp))): void
//
(* ****** ****** *)

implement
myftp_client_ls(chn) = let
//
val () = channeg_ssftp_ls(chn) in channel_recv(chn)
//
end // end of [myftp_client_ls]

(* ****** ****** *)

implement
myftp_client_cd(chn, Dir) = let
//
val () = channeg_ssftp_cd(chn, Dir) in channel_recv(chn)
//
end // end of [myftp_client_cd]

(* ****** ****** *)

implement
myftp_client_get(chn, Filename) = let
//
val () = channeg_ssftp_get(chn, Filename) in channel_recv(chn)
//
end // end of [myftp_client_get]

(* ****** ****** *)

implement
myftp_client_quit(chn) = let
//
val () =
  channeg_ssftp_quit(chn) in channeg_nil_close(chn)
//
end // end of [myftp_client_quit]

(* ****** ****** *)
//
assume pass_succ = ssconj(ssftp)
//
macdef SERVICE_NODE = $extval(atom, "'myftpd@localhost'")
//
(* ****** ****** *)

extern
fun
main0_erl
(
// argumentless
) : void = "mac#"
//
implement
main0_erl () =
{
//
val N = ref{int}(0)
//
implement
pass_client$password<> () = let
  val n = N[]
  val () = N[] := n+1
in
  if n <= 1 then "" else ("BU-CAS-CS520")
end
//
val chn = 
chansrvc_request
(
  $UN.cast{service(pass_init)}($tup(SERVICE_MYFTP,SERVICE_NODE))
) (* end of [val] *)
//
val-
~Some_vt(chn) = pass_client(chn)
//
val reply = myftp_client_ls(chn)
val ((*void*)) =
  println! ("myftp_client_ls: reply = ", $UN.cast{ERLval}(reply))
//
val reply = myftp_client_cd(chn, "..")
val ((*void*)) =
  println! ("myftp_client_cd: reply = ", $UN.cast{ERLval}(reply))
//
val reply = myftp_client_cd(chn, "myftp")
val ((*void*)) =
  println! ("myftp_client_cd: reply = ", $UN.cast{ERLval}(reply))
//
val reply = myftp_client_get(chn, "Makefile")
val ((*void*)) =
  println! ("myftp_client_get: reply = ", $UN.cast{ERLval}(reply))
//
val ((*void*)) = myftp_client_quit(chn)
//
} (* end of [main0_erl] *)

(* ****** ****** *)

(* end of [myftp_client.dats] *)
