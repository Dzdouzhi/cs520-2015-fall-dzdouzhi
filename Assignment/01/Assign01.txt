//
// Title: Principles of Programming Languages
// Number: CAS CS 520
// Semester: Fall 2015
// Class Time: MW 2:30-4:00
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//

######

Assigment 01

Due date: the 16th of September, 2015

######

Exercise 1.1: 10 points

Please give an implementation of 'binrev'
in Erlang.

Exercise 1.2: 10 points

Please give an implementation of 'count_ones'
in Erlang.

######

Exercise 2: 10 points

In the directory Example, there is a file of the
name 'fact_test_client.dats'. Please follow the
instructions given in Example/README to test this
file. Afterwards, please submit a text file that
records your testing session verbatim.

######

Exercise 3: 10 points

In the directory Example, there is a file of the
name 'binrev_test_server.dats'. Please implement a
client for testing 'binrev' by following the example
of 'fact_test_client.dats'. Afterwards, please submit
a text file that records your testing session verbatim.

###### end of [Assign01.txt] ######
