(*
** Server:
** for testing binrev
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

#define
ATS_EXTERN_PREFIX "binrev_test_server_"
#define
ATS_STATIC_PREFIX "_binrev_test_server_"

(* ****** ****** *)

%{^
%%
-module(binrev_test_server_dats).
%%
-export([main0_erl/0]).
%%
-compile(nowarn_unused_vars).
-compile(nowarn_unused_function).
%%
-export([ats2erlpre_cloref1_app/2]).
-export([ats2erlpre_cloref2_app/3]).
-export([libats2erl_session_chque_server/0]).
-export([libats2erl_session_chanpos_server/2]).
-export([libats2erl_session_channeg_server/2]).
-export([libats2erl_session_chansrvc_create_server/1]).
-export([libats2erl_session_chansrvc2_create_server/1]).
%%
-export([randint_server_loop/0]).
%%
-include("libatscc2erl_all.hrl").
-include("Session/mylibats2erl_all.hrl").
%%
%} // end of [%{]

(* ****** ****** *)

#include
"share/atspre_define.hats"

(* ****** ****** *)
//
#include
"{$LIBATSCC2ERL}/staloadall.hats"
//
staload
"{$LIBATSCC2ERL}/Session/SATS/basis.sats"
//
(* ****** ****** *)
//
staload UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)

%{^
%%
%%fun%%
randint(Pid, N) ->
  Pid!{self(),N}, receive {Pid,X} -> X end.
%%fun%%
randint_server() ->
  spawn(?MODULE, randint_server_loop, []).
%%fun%%
randint_server_loop() ->
  receive {Pid, N} -> Pid!{self(),random:uniform(N)-1} end, randint_server_loop().
%%
%} // end of [%{^]
extern
fun
randint : {n:pos} (pid, int(n)) -> natLt(n) = "mac#randint"

(* ****** ****** *)
//
typedef
sstest =
chsnd(int)::chrcv(int)::chsnd(bool)::chnil
//
typedef
service_test = service(sstest)
//
extern
fun
service_test(f: int -> int): service_test = "mac#%"
//
(* ****** ****** *)

macdef
SERVICE_BINREV_TEST = $extval(atom, "'SERVICE_BINREV_TEST'")

(* ****** ****** *)

fun
binrev
  (n:int): int = let
//
fun
aux(n: int, r: int): int =
(
if
n > 0
then let
  val n2 = n / 2
  val r2 = n - 2*n2
in
  aux (n2, 2*r + r2)
end
else (r)
) (* end of [aux] *)
//
in
  aux(n, 0)
end // end of [binrev]

(* ****** ****** *)
//
implement
service_test
  (ftest) = let
//
#define N 100
//
val Pid = $extfcall(pid, "randint_server")
//
fun
fserv
(
  chp: chanpos(sstest)
) : void = () where
{
  val n = randint(Pid, N)
  val () = channel_send(chp, n)
  val r1 = channel_recv(chp)
  val r2 = ftest(n)
  val () = channel_send(chp, r1 = r2)
  val () = chanpos_nil_wait(chp)
}
//
in
  chansrvc_create(lam(chp) => fserv(chp))
end // end of [service_binrev_test]
//
(* ****** ****** *)

extern
fun
main0_erl
(
// argumentless
) : void = "mac#"
//
implement
main0_erl () =
{
//
val () = chansrvc_register(SERVICE_BINREV_TEST, service_test(binrev))
//
} (* end of [main0_erl] *)

(* ****** ****** *)

(* end of [binrev_test_server.dats] *)
