(*
** Factorials
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

#define
ATS_EXTERN_PREFIX "fact_test_client_"
#define
ATS_STATIC_PREFIX "_fact_test_client_"

(* ****** ****** *)

%{^
%%
-module(fact_test_client_dats).
%%
-export([main0_erl/0]).
%%
-compile(nowarn_unused_vars).
-compile(nowarn_unused_function).
%%
-export([ats2erlpre_cloref1_app/2]).
-export([ats2erlpre_cloref2_app/3]).
-export([libats2erl_session_chque_server/0]).
-export([libats2erl_session_chanpos_server/2]).
-export([libats2erl_session_channeg_server/2]).
%%
-include("libatscc2erl_all.hrl").
-include("Session/mylibats2erl_all.hrl").
%%
%} // end of [%{]

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2ERL}/staloadall.hats"
//
staload
"{$LIBATSCC2ERL}/Session/SATS/basis.sats"
//
(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)

staload "./fact_test_server.dats"

(* ****** ****** *)
//
macdef
SERVICE_NODE =
  $extval(atom, "'fact_test_server@localhost'")
//
(* ****** ****** *)

fun fact(n:int): int = if n > 0 then n*fact(n-1) else 1

(* ****** ****** *)
//
extern
fun
fact_test_client(): bool
//
implement
fact_test_client() = let
//
val
chn = 
chansrvc_request
(
  $UN.cast{service_fact_test}($tup(SERVICE_FACT_TEST,SERVICE_NODE))
) (* end of [val] *)
//
val n = channel_recv(chn)
val () = println! ("fact_test_client: n = ", n)
val r = fact(n)
val () = channel_send(chn, r)
val () = println! ("fact_test_client: fact(", n, ") = ", r)
val result = channel_recv(chn)
val () = println! ("fact_test_client: result = ", result)
//
val ((*closed*)) = channel_close(chn)
//
in
  result
end // end of [fact_test_client]
//
(* ****** ****** *)

extern
fun
main0_erl
(
// argumentless
) : void = "mac#"
//
implement
main0_erl () =
{
//
val _ = fact_test_client()
val _ = fact_test_client()
val _ = fact_test_client()
//
} (* end of [main0_erl] *)

(* ****** ****** *)

(* end of [fact_test_client.dats] *)
