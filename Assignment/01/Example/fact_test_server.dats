(*
** Server for
** testing factorials
*)

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

#define
ATS_EXTERN_PREFIX "fact_test_server_"
#define
ATS_STATIC_PREFIX "_fact_test_server_"

(* ****** ****** *)

%{^
%%
-module(fact_test_server_dats).
%%
-export([main0_erl/0]).
%%
-compile(nowarn_unused_vars).
-compile(nowarn_unused_function).
%%
-export([ats2erlpre_cloref1_app/2]).
-export([ats2erlpre_cloref2_app/3]).
-export([libats2erl_session_chque_server/0]).
-export([libats2erl_session_chanpos_server/2]).
-export([libats2erl_session_channeg_server/2]).
-export([libats2erl_session_chansrvc_create_server/1]).
-export([libats2erl_session_chansrvc2_create_server/1]).
%%
-export([randint_server_loop/0]).
%%
-include("libatscc2erl_all.hrl").
-include("Session/mylibats2erl_all.hrl").
%%
%} // end of [%{]

(* ****** ****** *)

#include
"share/atspre_define.hats"

(* ****** ****** *)
//
#include
"{$LIBATSCC2ERL}/staloadall.hats"
//
staload
"{$LIBATSCC2ERL}/Session/SATS/basis.sats"
//
(* ****** ****** *)
//
staload UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)

%{^
%%
%%fun%%
randint(Pid, N) ->
  Pid!{self(),N}, receive {Pid,X} -> X end.
%%fun%%
randint_server() ->
  spawn(?MODULE, randint_server_loop, []).
%%fun%%
randint_server_loop() ->
  receive {Pid, N} -> Pid!{self(),random:uniform(N)-1} end, randint_server_loop().
%%
%} // end of [%{^]
extern
fun
randint : {n:pos} (pid, int(n)) -> natLt(n) = "mac#randint"

(* ****** ****** *)
//
typedef
ssfact_test =
chsnd(int)::chrcv(int)::chsnd(bool)::chnil
//
typedef
service_fact_test = service(ssfact_test)
//
extern
fun service_fact_test(): service_fact_test = "mac#%"
//
(* ****** ****** *)

macdef
SERVICE_FACT_TEST = $extval(atom, "'SERVICE_FACT_TEST'")

(* ****** ****** *)

fun fact(n:int) = 
  (fix f(n:int):int => if n > 0 then n*f(n-1) else 1)(n)

(* ****** ****** *)
//
implement
service_fact_test() = let
//
#define N 100
//
val Pid = $extfcall(pid, "randint_server")
//
fun
fserv
(
  chp: chanpos(ssfact_test)
) : void = () where
{
  val n = randint(Pid, N)
  val () = channel_send(chp, n)
  val r1 = channel_recv(chp)
  val r2 = fact(n)
  val () = channel_send(chp, r1 = r2)
  val () = chanpos_nil_wait(chp)
}
//
in
  chansrvc_create(lam(chp) => fserv(chp))
end // end of [service_fact_test]
//
(* ****** ****** *)

extern
fun
main0_erl
(
// argumentless
) : void = "mac#"
//
implement
main0_erl () =
{
//
val () = chansrvc_register(SERVICE_FACT_TEST, service_fact_test())
//
} (* end of [main0_erl] *)

(* ****** ****** *)

(* end of [fact_test_server.dats] *)
