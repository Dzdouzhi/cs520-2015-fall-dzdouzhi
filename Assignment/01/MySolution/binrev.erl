-module(binrev).
-export([binrev/2]).
binrev(X,Y) when X>1 ->
binrev(trunc(X/2),Y*2+X rem 2*2);
binrev(X,Y)-> Y+1.