-module(count_ones).
-export([count_ones/2]).
count_ones(X,Y) when X>1 ->count_ones(trunc(X/2),Y+X rem 2);
count_ones(X,Y) -> Y+1.