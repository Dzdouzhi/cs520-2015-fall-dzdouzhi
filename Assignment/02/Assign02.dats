(*
** Assignment 2
** It is due the 23rd of September, 2015
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// HX:
// A natural number can be represented
// as a list of digits. For instance, 12345
// can be represented as [5, 4, 3, 2, 1].
//
// Please implement addition and multiplication
// operations on mynat
//
(* ****** ****** *)

typedef mynat = list0 (int)

(* ****** ****** *)
//
extern
fun // 10 points
add_mynat_mynat
  (x: mynat, y: mynat): mynat // =x+y
//
extern
fun // 10 points
mul_mynat_mynat
  (x: mynat, y: mynat): mynat // =x*y
//
(* ****** ****** *)

overload + with add_mynat_mynat of 10
overload * with mul_mynat_mynat of 10

(* ****** ****** *)
//
// Please implement comparison on mynat
//
extern
fun // 10 points
compare_mynat_mynat(mynat, mynat): int
//
// compare(x, y) =  0 if x = y
// compare(x, y) =  1 if x > y
// compare(x, y) = -1 if x < y
//
overload compare with compare_mynat_mynat of 10
//
(* ****** ****** *)
//
// HX: myint is for integers
//
datatype myint =
  | Nil // = 0
  | Pos of mynat // = mynat != 0
  | Neg of mynat // = -(mynat) != 0

(* ****** ****** *)
//
// Please implement addition, subtraction
// and multiplication operations on myint
//
extern
fun // 10 points
add_myint_myint
  (x: myint, y: myint): myint // = x+y
//
extern
fun // 10 points
sub_myint_myint
  (x: myint, y: myint): myint // = x-y
//
extern
fun // 10 points
mul_myint_myint
  (x: myint, y: myint): myint // = x*y
//
(* ****** ****** *)

overload + with add_myint_myint
overload - with sub_myint_myint
overload * with mul_myint_myint

(* ****** ****** *)
//
// There are for bonus points:
//
extern
fun // 10 points (bonus)
div_mynat_mynat // division
  (x: mynat, y: mynat): mynat // = x/y
extern
fun // 10 points (bonus)
mod_mynat_mynat // modulo operation
  (x: mynat, y: mynat): mynat // = x%y
//
overload / with div_mynat_mynat
overload % with mod_mynat_mynat
//
(* ****** ****** *)
//
extern
fun // 10 points (bonus)
sqrt_mynat(x: mynat): mynat
//
overload sqrt with sqrt_mynat
//
// Note that sqrt(x) is the largest
// natural number n such that n*n <= x
//
(* ****** ****** *)

(*
//
// HX:
// Following code is provided
// for the purpose of debugging/testing
//
*)

(* ****** ****** *)
//
extern
fun{} // it is given
print_mynat (mynat): void
extern
fun{} // it is given
fprint_mynat (FILEref, mynat): void
//
overload print with print_mynat
overload fprint with fprint_mynat
//
(* ****** ****** *)
//
extern
fun{} // it is given
print_myint (myint): void
extern
fun{} // it is given
fprint_myint (FILEref, myint): void
//
overload print with print_myint
overload fprint with fprint_myint
//
(* ****** ****** *)
//
implement
print_mynat<>
  (ds) = fprint_mynat(stdout_ref, ds)
//
implement
fprint_mynat<>
  (out, ds) =
(
  fprint(out, "[");
  fprint_list0_sep(out, list0_reverse(ds), "");
  fprint(out, "]");
)
//
(* ****** ****** *)
//
implement
print_myint<>
  (ds) = fprint_myint(stdout_ref, ds)
//
(*
#codegen2(fprint, myint, fprint_myint)
*)
//
#include "./Assign02_fprint_myint.hats"
//
(* ****** ****** *)
//
implement
fprint_myint$lpar<>(out) = ()
implement
fprint_myint$rpar<>(out) = ()
//
implement
fprint_myint$carg<mynat> = fprint_mynat<>
//
(* ****** ****** *)
//
extern
fun{} base_get(): intGte(2)
//
implement{} base_get() = 10
//
(* ****** ****** *)
//
extern
fun{}
randgen_mynat (n: intGte(0)): mynat
//
(* ****** ****** *)
//
staload
TIME = "libc/SATS/time.sats"
staload
STDLIB = "libc/SATS/stdlib.sats"
//
implement
{}(*tmp*)
randgen_mynat (n) = let
  val base = base_get()
in
  list0_tabulate<int> (n, lam(i) => $UNSAFE.cast{int}($STDLIB.random()) % base)
end // end of [randgen_mynat]
//
(* ****** ****** *)

(* end of [Assign02.dats] *)
