(*
** Solution to Assign02
*)

(* ****** ****** *)

#include "./../Assign02.dats"

(* ****** ****** *)

extern
fun{}
int2mynat (intGte(0)): mynat

(* ****** ****** *)

implement
{}(*tmp*)
int2mynat(n) = let
//
val base = base_get()
//
fun
aux(n: int): mynat =
(
//
if
n >= 1
then let
  val d = n%base
  val ds = aux(n/base)
in
  list0_cons(d, ds)
end // end of [then]
else list0_nil((*void*))
//
) (* end of [aux] *)
//
in
  aux(n)
end // end of [int2mynat]

(* ****** ****** *)
//
extern
fun
succ_mynat (mynat): mynat
//
overload succ with succ_mynat  
//
(* ****** ****** *)

implement
succ_mynat(ds) = let
//
val base = base_get()
//
in
//
case+ ds of
| nil0 () =>
    list0_sing(1)
| cons0(d, ds) => let
    val d1 = succ(d)
  in
    if d1 >= base
      then cons0(0, succ(ds)) else cons0(d1, ds)
    // end of [if]
  end // end of [cons0]
//
end // end of [succ_mynat]

(* ****** ****** *)
//
extern
fun
add_mynat_int
  (mynat, intGte(0)): mynat
//
overload + with add_mynat_int of 10
//
extern
fun
mul_int_mynat
  (intGte(0), mynat): mynat
//
overload * with mul_int_mynat of 10
//
(* ****** ****** *)

(*
implement
add_mynat_int
  (ds, n) = let
//
val base = base_get()
//
in
//
if
n > 0
then (
//
case+ ds of
| nil0 () =>
    int2mynat(n)
| cons0 (d, ds) => let
    val dn = d + n
    val d1 = dn % base
    val d2 = dn / base
    val d2 =
      $UNSAFE.cast{intGte(0)}(d2)
    // end of [val]
  in
    cons0 (d1, add_mynat_int(ds, d2))
  end // end of [cons0]
//
) (* end of [then] *)
else ds // end of [else]
//
end // end of [add_mynat_int]
*)

(* ****** ****** *)

(*
implement
mul_int_mynat
  (n, x) = let
//
fun
loop
(
  n:int, x:mynat, res:mynat
) : mynat = (
//
if
n >= 2
then let
  val n2 = half(n)
  val res =
  (
    if n=2*n2 then res else x+res
  ) : mynat // end of [val]
in
  loop(n2, x+x, res)
end // end of [then]
else (
  if n = 0 then res else x + res
) (* end of [else] *)
//
) (* end of [loop] *)
//
in
  loop(n, x, int2mynat(0))
end // end of [mul_int_mynat]
*)

(* ****** ****** *)

#ifndef MAIN_NONE
//
implement
main0 () = () where
{
//
val () =
  $STDLIB.srandom($UNSAFE.cast($TIME.time()))
//
val x =
g0ofg1
(
  $list{int}(1, 2, 3, 4, 5)
) (* end of [val] *)
val y = Pos(x) and z = Neg(x)
val () = println! ("(", y,  " + ", z, ") = ", 0)
//
val x = randgen_mynat(10)
val y = Pos(x) and z = Neg(x)
val () = println! ("(", y,  " + ", z, ") = ", 0)
//
(*
val m = randgen_mynat(100)
val n = randgen_mynat(200)
val () = assertloc (compare(m+n, n+m) = 0)
val () = assertloc (compare(m*n, n*m) = 0)
val () = assertloc (compare((n+1)*(n+1), n*n+2*n+1) = 0)
*)
//
} (* end of [main0] *)
//
#endif // #ifdef MAIN_NONE

(* ****** ****** *)

(* end of [Assign02_sol.dats] *)
