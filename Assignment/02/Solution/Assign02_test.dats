(* ****** ****** *)
//
#include
"./../MySolution/Assign02_sol.dats"
//
(* ****** ****** *)
//
fun succ(n:mynat): mynat = n+1
//
fun pred(n:mynat): mynat =
  let val-Pos(n1) = Pos(n) - Pos(int2mynat(1)) in n1 end
//
(* ****** ****** *)
//
implement
main0 () = () where
{
//
val () =
  $STDLIB.srandom($UNSAFE.cast($TIME.time()))
//
val x =
g0ofg1
(
  $list{int}(1, 2, 3, 4, 5)
) (* end of [val] *)
val y = Pos(x) and z = Neg(x)
val () = println! ("(", y,  " + ", z, ") = ", 0)
//
val x = randgen_mynat(10)
val y = Pos(x) and z = Neg(x)
val () = println! ("(", y,  " + ", z, ") = ", 0)
//
val m = randgen_mynat(100)
val n = randgen_mynat(100)
//
val () =
assertloc
  (compare((m+n)*(m+n), m*m+2*m*n+n*n) = 0)
//
val x =
  Pos(m) - Pos(n)
//
val-Pos(xx) = x * x
val () = assertloc(compare(xx+2*m*n, m*m + n*n) = 0)
//
val () = assertloc(compare(m, int2mynat(2)) >= 0)
val () = assertloc(compare((m+1)*(m+1)/m, m + 2) = 0)
val () = assertloc(compare((m+1)*(m+1)%m, int2mynat(1)) = 0)
//
val () = assertloc(compare(sqrt(m*m), m) = 0)
val () = assertloc(compare(sqrt(succ(m*m)), m) = 0)
val () = assertloc(compare(sqrt(pred(m*m)), pred(m)) = 0)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Assign02_test.dats] *)
