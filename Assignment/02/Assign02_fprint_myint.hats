(* ****** ****** *)
//
extern
fun{}
fprint_myint$Nil: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Pos: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Neg: $d2ctype(fprint_myint<>)
//
(* ****** ****** *)
//
implement{}
fprint_myint
  (out, arg0) =
(
case+ arg0 of
| Nil _ => fprint_myint$Nil<>(out, arg0)
| Pos _ => fprint_myint$Pos<>(out, arg0)
| Neg _ => fprint_myint$Neg<>(out, arg0)
)
//
(* ****** ****** *)
//
extern
fun{}
fprint_myint$sep: (FILEref) -> void
implement{}
fprint_myint$sep(out) = fprint(out, ",")
//
extern
fun{}
fprint_myint$lpar: (FILEref) -> void
implement{}
fprint_myint$lpar(out) = fprint(out, "(")
//
extern
fun{}
fprint_myint$rpar: (FILEref) -> void
implement{}
fprint_myint$rpar(out) = fprint(out, ")")
//
extern
fun{a:t0p}
fprint_myint$carg: (FILEref, INV(a)) -> void
implement{a}
fprint_myint$carg(out, arg) = fprint_val<a>(out, arg)
//
(* ****** ****** *)
//
extern
fun{}
fprint_myint$Nil$con: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Nil$lpar: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Nil$rpar: $d2ctype(fprint_myint<>)
//
implement{}
fprint_myint$Nil(out, arg0) = 
{
//
val () = fprint_myint$Nil$con<>(out, arg0)
val () = fprint_myint$Nil$lpar<>(out, arg0)
val () = fprint_myint$Nil$rpar<>(out, arg0)
//
}
implement{}
fprint_myint$Nil$con(out, _) = fprint(out, "Nil")
implement{}
fprint_myint$Nil$lpar(out, _) = fprint_myint$lpar(out)
implement{}
fprint_myint$Nil$rpar(out, _) = fprint_myint$rpar(out)
//
extern
fun{}
fprint_myint$Pos$con: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Pos$lpar: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Pos$rpar: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Pos$arg1: $d2ctype(fprint_myint<>)
//
implement{}
fprint_myint$Pos(out, arg0) = 
{
//
val () = fprint_myint$Pos$con<>(out, arg0)
val () = fprint_myint$Pos$lpar<>(out, arg0)
val () = fprint_myint$Pos$arg1<>(out, arg0)
val () = fprint_myint$Pos$rpar<>(out, arg0)
//
}
implement{}
fprint_myint$Pos$con(out, _) = fprint(out, "Pos")
implement{}
fprint_myint$Pos$lpar(out, _) = fprint_myint$lpar(out)
implement{}
fprint_myint$Pos$rpar(out, _) = fprint_myint$rpar(out)
implement{}
fprint_myint$Pos$arg1(out, arg0) =
  let val-Pos(arg1) = arg0 in fprint_myint$carg(out, arg1) end
//
extern
fun{}
fprint_myint$Neg$con: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Neg$lpar: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Neg$rpar: $d2ctype(fprint_myint<>)
extern
fun{}
fprint_myint$Neg$arg1: $d2ctype(fprint_myint<>)
//
implement{}
fprint_myint$Neg(out, arg0) = 
{
//
val () = fprint_myint$Neg$con<>(out, arg0)
val () = fprint_myint$Neg$lpar<>(out, arg0)
val () = fprint_myint$Neg$arg1<>(out, arg0)
val () = fprint_myint$Neg$rpar<>(out, arg0)
//
}
implement{}
fprint_myint$Neg$con(out, _) = fprint(out, "Neg")
implement{}
fprint_myint$Neg$lpar(out, _) = fprint_myint$lpar(out)
implement{}
fprint_myint$Neg$rpar(out, _) = fprint_myint$rpar(out)
implement{}
fprint_myint$Neg$arg1(out, arg0) =
  let val-Neg(arg1) = arg0 in fprint_myint$carg(out, arg1) end
//
(* ****** ****** *)
