(*
** Solution to Assign02-2
*)

(* ****** ****** *)

#include "./../Assign02-2.dats"

(* ****** ****** *)

assume myset = list0(int) // ordered

(* ****** ****** *)

implement
myset_nil() = list0_nil((*void*))

(* ****** ****** *)
//
extern
fun
list2myset (list0(int)): myset
//
implement
list2myset(xs) =
list0_foldleft
  (xs, list0_nil(), lam(res, x) => myset_insert(res, x))
//
(* ****** ****** *)

#ifndef MAIN_NONE

implement
main0 () = () where
{
//
val () = println! ("Assign02-2: testing starts")
//
(*
// HX: Please put your testing code here!
*)
//
val xs = $list{int}(1,2,3)
val xs = list2myset(g0ofg1(xs))
val () = myset_foreach(xs, lam(x) => println!(x))
//
val () = println! ("Assign02-2: testing finishes")
//
} (* end of [main0] *)

#endif // #ifdef MAIN_NONE

(* ****** ****** *)

(* end of [Assign02-2_sol.dats] *)
