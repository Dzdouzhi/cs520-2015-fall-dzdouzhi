(*
** Assignment 2-2
** It is due Monday, 28th of September, 2015
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

abstype myset = ptr // for sets of integers

(* ****** ****** *)
//
// Please give an implementation of [myset]
// that represents each integer set as an ordered
// list of integers. For instance, the set {3,1,2}
// is represented as [1,2,3].
//
(* ****** ****** *)
//
// It is given
//
extern
fun
myset_nil (): myset
//
(* ****** ****** *)
//
extern
fun // 5 points // membership test
myset_is_member (xs: myset, x0: int): bool
//
(* ****** ****** *)

extern
// 5 points // inserts [x0] into [xs]
fun myset_insert (xs: myset, x0: int): myset
extern 
// 5 points // removes [x0] from [xs]
fun myset_remove (xs: myset, x0: int): myset

(* ****** ****** *)
//
extern // 10 points
// computes the union of [xs] and [ys]
fun myset_union (xs: myset, ys: myset): myset
extern // 10 points 
// computes the intersection of [xs] and [ys]
fun myset_intersect (xs: myset, ys: myset): myset
//
(* ****** ****** *)
//
extern
// Filtering out all of the elements satisfying the
// predicate [pred]
fun // 10 points
myset_filter (xs: myset, pred: int -<cloref1> bool): myset
//
(* ****** ****** *)
//
extern
// Applying the procedure [proc] to each element in [xs]
fun // 10 points
myset_foreach (xs: myset, proc: int -<cloref1> void): void
//
(* ****** ****** *)

(* end of [Assign02-2.dats] *)
