(*
** A sample for illustration
*)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

(* ****** ****** *)
//
// Exercise 1: 10 points
//
extern fun binrev : int -> int
//
(* ****** ****** *)

implement
main0 () =
{
  val () = println! ("binrev(100) = ", binrev(100))
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Sample.dats] *)