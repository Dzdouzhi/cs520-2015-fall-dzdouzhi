#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
fun
count_ones(n: int, m: int): int=
  if n>1 then count_ones(n/2, m+n%2) else m+1
//
(* ****** ****** *)
//
val n = 10 and m = 0
val () = println! ("count_ones(", n, ") = ", count_ones(n,m))
//
(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]
