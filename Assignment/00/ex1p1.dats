#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
fun
Fib(n: int, f1: int, f2: int): int =
if n>0 then Fib(n-1,f2,f1+f2) else f1
//
(* ****** ****** *)
//
val n = 5 and f1=0 and f2=1
val () = println! ("Fib(", n,") = ", Fib(n,f1,f2))
//
(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]
