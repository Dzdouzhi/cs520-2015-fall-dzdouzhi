(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

extern
fun binrev (n:int): int

(* ****** ****** *)

implement
binrev (n) = let
//
fun
aux
(
  n: int, r: int
) : int =
(
if
n > 0
then let
  val n2 = n / 2
  val r2 = n - 2*n2
in
  aux (n2, 2*r + r2)
end
else (r)
) (* end of [aux] *)
//
in
  aux(n, 0)
end // end of [binrev]

(* ****** ****** *)

implement
main0 () =
{
val () = println! ("binrev(7) = ", binrev(7))
val () = println! ("binrev(10) = ", binrev(10))
val () = println! ("binrev(16) = ", binrev(16))
} (* end of [main0] *)

(* ****** ****** *)

(* end of [binrev] *)
