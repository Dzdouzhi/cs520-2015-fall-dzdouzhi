(*
<p>
The Fibonacci numbers can be computed as follows:
<pre>
fib(0) = 0; fib(1) = 1; fib(n) = fib(n-1) + fib(n-2) for n >= 2.
</pre>
Please write a function to compute fib(20). You should make it possible
to test your code on-line.
</p>
*)

(* ****** ****** *)

#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
extern
fun
fib (n:int): int
//
(* ****** ****** *)

implement
fib (n) = let
//
fun
aux
(
  n: int, r0: int, r1: int
) : int =
(
if
n >= 2
then aux (n-1, r1, r0+r1)
else (if n = 0 then r0 else r1)
) (* end of [aux] *)
//
in
  aux(n, 0, 1)
end // end of [fib]

(* ****** ****** *)

val () = println! ("fib(7) = ", fib(7))
val () = println! ("fib(10) = ", fib(10))
val () = println! ("fib(15) = ", fib(15))
val () = println! ("fib(16) = ", fib(16))

(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]

(* ****** ****** *)

(* end of [fib.dats] *)
