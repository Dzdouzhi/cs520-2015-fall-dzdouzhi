(*
<p>
Please write a function of the name binrev that finds the
integer whose binary representation is the reverse of the
binary representation of a given integer. For instance,
<pre>
binrev(0) = 0
binrev(1) = 1
binrev(2) = 1 // 2 -> 10 -> 01 -> 1
binrev(3) = 3 // 3 -> 11 -> 11 -> 3
binrev(10) = 5 // 10 -> 1010 -> 0101 -> 5
</pre>
You should make it possible to test your code on-line.
</p>
*)

(* ****** ****** *)

#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
extern
fun
binrev (n:int): int
//
(* ****** ****** *)

implement
binrev (n) = let
//
fun
aux
(
  n: int, r: int
) : int =
(
if
n > 0
then let
  val n2 = n / 2
  val r2 = n - 2*n2
in
  aux (n2, 2*r + r2)
end
else (r)
) (* end of [aux] *)
//
in
  aux(n, 0)
end // end of [binrev]

(* ****** ****** *)

val () = println! ("binrev(7) = ", binrev(7))
val () = println! ("binrev(10) = ", binrev(10))
val () = println! ("binrev(15) = ", binrev(15))
val () = println! ("binrev(16) = ", binrev(16))

(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]

(* ****** ****** *)

(* end of [binrev.dats] *)
