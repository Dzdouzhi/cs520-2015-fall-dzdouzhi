(*
<p>
Please write a function of the name count_ones that
counts the number of 1's in the binary representation
of a given natural number. For instance,
<pre>
count_ones(0) = 0
count_ones(1) = 1
count_ones(2) = 1
count_ones(3) = 2
count_ones(10) = 2
</pre>
You should make it possible to test your code on-line.
</p>
*)

(* ****** ****** *)

#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)

extern
fun
count_ones (n:int): int

(* ****** ****** *)

implement
count_ones (n) = let
//
fun
aux
(
  n: int, c: int
) : int =
(
if
n > 0
then let
  val n2 = n / 2
in
  aux (n2, c + n - 2*n2)
end
else (c)
) (* end of [aux] *)
//
in
  aux(n, 0)
end // end of [count_ones]

(* ****** ****** *)

val () = println! ("count_ones(7) = ", count_ones(7))
val () = println! ("count_ones(10) = ", count_ones(10))
val () = println! ("count_ones(15) = ", count_ones(15))
val () = println! ("count_ones(16) = ", count_ones(16))

(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]

(* ****** ****** *)

(* end of [count_ones.dats] *)
