(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

extern
fun fib (n:int): int

(* ****** ****** *)

implement
fib (n) = let
//
fun
aux
(
  n: int, r0: int, r1: int
) : int =
(
if
n >= 2
then aux (n-1, r1, r0+r1)
else (if n = 0 then r0 else r1)
) (* end of [aux] *)
//
in
  aux(n, 0, 1)
end // end of [fib]

(* ****** ****** *)

implement
main0 () =
{
val () = println! ("fib(7) = ", fib(7))
val () = println! ("fib(10) = ", fib(10))
val () = println! ("fib(15) = ", fib(15))
val () = println! ("fib(16) = ", fib(16))
} (* end of [main0] *)

(* ****** ****** *)

(* end of [fib.dats] *)
