(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

extern
fun
count_ones (n:int): int

(* ****** ****** *)

implement
count_ones (n) = let
//
fun
aux
(
  n: int, c: int
) : int =
(
if
n > 0
then let
  val n2 = n / 2
in
  aux (n2, c + n - 2*n2)
end
else (c)
) (* end of [aux] *)
//
in
  aux(n, 0)
end // end of [count_ones]

(* ****** ****** *)

implement
main0 () =
{
val () = println! ("count_ones(7) = ", count_ones(7))
val () = println! ("count_ones(10) = ", count_ones(10))
val () = println! ("count_ones(15) = ", count_ones(15))
val () = println! ("count_ones(16) = ", count_ones(16))
} (* end of [main0] *)

(* ****** ****** *)

(* end of [count_ones.dats] *)
