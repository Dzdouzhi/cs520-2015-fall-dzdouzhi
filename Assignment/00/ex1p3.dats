#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)

staload
"{$LIBATSCC2JS}/SATS/print.sats"

(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
fun
binrev(n:int, res:int):int=
  if n>1 then binrev(n/2, res*2+n%2*2) else res+1
//
(* ****** ****** *)
//
val n = 2 and res=0
val () = println! ("binrev(", n, ") = ", binrev(n,res))
//
(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]
