(* ****** ****** *)

#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
staload
"{$LIBATSCC2JS}/SATS/print.sats"
//
(* ****** ****** *)

#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "my_dynload"

(* ****** ****** *)
//
val () = println! (100 << 1)
val () = println! (100 >> 1)
val () = println! ($UN.cast{uint}(100) >> 1)
//
val () = println! (100 lor 0x1)
val () = println! (101 lxor 0x1)
//
val () = println! (100 land 0x1)
val () = println! (101 land 0x1)
//
(* ****** ****** *)

%{$
//
ats2jspre_the_print_store_clear();
my_dynload();
alert(ats2jspre_the_print_store_join());
//
%} // end of [%{$]
